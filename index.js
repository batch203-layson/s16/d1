// console.log("Hello World");

// [SECTION] Arithmetic Operators 

    let x = 100;
    let y = 25;

    //Addition
    let sum = x + y;
    console.log("Result of addition operator: " +sum);

    // Subtraction
    let difference = x - y;
    console.log("Result of subtraction operator: " + difference);

    // Multiplication
    let product = x * y;
    console.log("Result of multiplication operator: " + product);
    
    // Division
    let quotient = x / y;
    console.log("Result of division operator: " + quotient);
    
    // Modulo - > Remainder - > can be used for odd and even - > also if divisible by 2 - > also to know if it is divisible by itself
    let modulo = x % y;
    console.log("Result of modulo operator: " + modulo);

    // [SECTION] Assignment Operator
        // Basic Assignment Operator (=)
        // The assignment operator assigns the value of the "right hand" operand to a variable.
        let assignmentNumber = 8;
        console.log("The current value of the assignmentNumber variable: " +assignmentNumber);

        // Addition Assignment Operator
                            // 8 + 2
        assignmentNumber = assignmentNumber + 2;
        console.log("Result of addition assignmentNumber operator: " +assignmentNumber);

        // Subtraction/Multiplication/Division (-=, *=, /=)
        assignmentNumber -= 2;
        console.log("Result of subtraction assignment operator: " + assignmentNumber);

        assignmentNumber *= 2;
        console.log("Result of the multiplication assignment operator: " + assignmentNumber);

        assignmentNumber /=2;
        console.log("Result of the division assignment operator: " +assignmentNumber);

    // [SECTION] PEMDAS (Order of Operations)
    //  Multiple Operators and Parenthesis
    //  When multiple operators are applied in a single statement it follows PEMDAS
    //  The operations were done in the following orders 
    let mdas = 1 + 2 - 3 * 4 / 5;
    console.log("Result of mdas operation: " +mdas);

    // The operations were done in the following order:
    /*
        1. 4 / 5 = 0.8 | 1 + ( 2 - 3 ) * 0.8
        2. 2 - 3 = -1 | 1 + (-1) * 0.8
        3. -1 * 0.8 = -0.8
        4. 1 - 0.8 = 0.20 //result
    */

        
    // console.log("Result of pemdas operation: " +pemdas); //.20
    /* let pemdas = 1 + (2 - 3) * (4/5); */
    let pemdas = (1+(2 - 3)) * (4/5);
    console.log("Result of pemdas operation: " +pemdas);



    // [SECTION] Increment and Decrement
    // Operators that add or subtract a values by 1 and reassign the value of the variable where the increment/decrement was applied.
    let z = 1;
    // The of "z" is added by a value of one before returning the value and storing it in the variable increment
    let increment = ++z;
    console.log("Result of pre-increment: " + increment);
    console.log("Result of pre-increment for z: " + z);

    // Javascript read the code from top to boittom and left to right
    // The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one.
    increment = z++;
    // The value of "z" is at 2 before it was incremented.
    console.log("Result of post-increment: " + increment);
    console.log("Result of the post-increment for z: " + z);


    let decrement = --z;

    console.log("Result of pre-decrement: " +decrement);
    console.log("Result of pre-decrement for z: " +z);

    
    decrement = z--;
    console.log("Result of post-decrement: " +decrement);
    console.log("Result of post-decrement for z: " +z);

// [SECTION] Type Coercion
// converts one data type to another
// is the automatic conversion of values from one data type to another

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion);


/* 
- The result is a number
- The boolean "true" is also associated with the value of 1

*/

let numE = true + 1;
console.log(numE);


/* 
- The result is a number
- The boolean "false" is also associated with the value of 0.
*/
let numF = false + 1;
console.log(numF);


// [SECTION] Comparison Operators
// Comparision operators are sued to evaluate and compare the left and right operands
//  After evaluation, it returns a boolean value

let juan = "juan";

// Equality Operator (==)
/* 
- Checks whether the operands are equal/have the same content.
*/ 

console.log(1 == 1); //true

console.log(1 == 2); //false



console.log(1 == '1'); //true



console.log(1 == 'one'); //false



console.log(1 == 'false'); //true



console.log('juan' == 'juan'); //false


// Compares a string with the variable  juan
console.log('juan' == juan); //false

// Inequality Operator
/* 
 - Checks whether the operands are not equal/have the different value.
 - Attempts to CONVERT AND COMPARE operand of different data type.
*/


console.log(1 != 1); //false

console.log(1 != 2); //true

console.log(0 != false); // false

console.log('juan' != 'juan'); // false

console.log('juan' != juan); // false


// Strict Equality operation (==)

// Check whether the operands are equal/have the same content

//Also COMPARES the DATA TYPES of 2 values

console.log(1==1); // true
console.log(1==2); // false
console.log(1=='1'); // false
console.log(0==false); //false

console.log('juan'=='juan'); //true

console.log('juan'==juan); //true

// Strict Inequality Operator (!==)
/* 
- checks whether the oerands are not equal / don't have the same content
- also compares the data types of 2 values
*/

console.log(1 !==1); // false
console.log(1 !==2); // true
console.log(1 !=='1'); // true
console.log(0 !== false); //true
console.log('juan'!=='juan');//false
console.log('juan'!==juan);//false


//[SECTION] Relational Operators
//Some comparision operators check whether one value is greater or less than to the other value

let a = 50;
let b = 65;
// GT or Greater Than Operator ()
let isGreaterThan = a > b;
console.log(isGreaterThan); //false


// LT or Less Than Operator
let isLessThan = a < b;
console.log(isLessThan); //True

// GTE or Greater Than or Equal (>=)
let isGTorEqual = a >= b; //50 >= 65
console.log(isGTorEqual); //false

// LTE or Less Than or Equal (<=)
let isToOrEqual = a <= b;
console.log(isToOrEqual); //true

let  numStr = "30";
console.log(a > numStr); //true - forced coercion to change the string to number.

let str = "twenty";
console.log(b>=str); false
//Since the string is not numeric, the string was not converted to a number. 65 >= NaN (Not a Number)



// [SECTION] Logical Operators
// Logical Operators allow us to be more specific in the logical combination of conditions and evaluations. it return boolean

let isLegalAge = true;
let isRegistered = true;

// Logical AND operator (&& - Double Ampersand)
/* let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " +allRequirementsMet);


let isLegalAge = false;
let isRegistered = true;
*/

// Logical AND operator (&& - Double Ampersand)
/* let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " +allRequirementsMet);



let isLegalAge = false;
let isRegistered = false;
*/

// Logical AND operator (&& - Double Ampersand)
/*  let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " +allRequirementsMet);*/





// Logical OR Operator(|| Double Pipe)
/* let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);*/




// Logical OR Operator(|| Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);


// Logical Not Operator(! - Exclamation Point);
// Returns the opposite value
/* let someRequirementsNotMet =! isRegistered;
console.log("Result of logical Not Operator: " + someRequirementsNotMet);
*/ 

// Logical Not Operator(! - Exclamation Point);
// Returns the opposite value
let someRequirementsNotMet =! isLegalAge;
console.log("Result of logical Not Operator: " + someRequirementsNotMet);

